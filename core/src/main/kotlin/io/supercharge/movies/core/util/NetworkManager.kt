package io.supercharge.movies.core.util

interface NetworkManager {
    fun isConnectionAvailable(): Boolean
}