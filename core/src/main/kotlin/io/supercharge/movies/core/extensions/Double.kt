package io.supercharge.movies.core.extensions

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

val Double.toStringDecimalWithFraction: String
    get() = doubleToStringDecimalWithFraction(this)

val Double.toStringDecimal: String
    get() = doubleToStringNoDecimal(this)


fun Double.formatBalance(fractions: Int?): String {
    val otherSymbols = DecimalFormatSymbols(Locale.US)
    otherSymbols.groupingSeparator = ','
    var formatter = DecimalFormat("#,###,##0", otherSymbols)
    fractions?.let {
        if (it > 0) {
            formatter = DecimalFormat("#,###,##0.${"0".repeat(it)}", otherSymbols)
        }
    }

    return formatter.format(this)

}


fun doubleToStringDecimalWithFraction(d: Double): String {
    val otherSymbols = DecimalFormatSymbols(Locale.US)
    otherSymbols.decimalSeparator = '.'
    otherSymbols.groupingSeparator = ','
    val formatter = DecimalFormat("#,###,##0.00", otherSymbols)
    return formatter.format(d)
}

fun doubleToStringNoDecimal(d: Double): String {
    val otherSymbols = DecimalFormatSymbols(Locale.US)
    otherSymbols.groupingSeparator = '.'
    val formatter = DecimalFormat("#,###,##0", otherSymbols)
    return formatter.format(d)
}

