package io.supercharge.movies.core.extensions

fun Int?.isNullorZero() : Boolean {
    return this == null || this == 0
}