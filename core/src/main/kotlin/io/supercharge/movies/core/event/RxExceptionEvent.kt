package io.supercharge.movies.core.event

/**
 * Used to pass an rx stream error up to the presentation layer.
 * This is a good way to to log rx stream errors.
 */
class RxExceptionEvent(var error: Throwable)