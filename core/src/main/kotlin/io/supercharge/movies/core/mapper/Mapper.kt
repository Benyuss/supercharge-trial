package io.supercharge.movies.core.mapper

import io.supercharge.movies.core.entities.Optional

/**
 * Mapper interface to exchange models between different layers.
 */
interface Mapper<E, T> {

    fun mapFrom(from: E): T

    fun mapTo(from: T): E {
        TODO("Optionally implemented")
    }

    fun mapFromList(from: List<E>): List<T> {
        return from.map { response -> mapFrom(response) }
    }

    fun mapToList(from: List<T>): List<E> {
        return from.map { response -> mapTo(response) }
    }

    fun mapFromOptional(from: E): Optional<T> {
        return Optional(mapFrom(from))
    }

    fun mapToOptional(from: T): Optional<E> {
        return Optional(mapTo(from))
    }
}