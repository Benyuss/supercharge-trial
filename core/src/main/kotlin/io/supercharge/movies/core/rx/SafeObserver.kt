package io.supercharge.movies.core.rx

import io.reactivex.observers.DisposableObserver
import io.supercharge.movies.core.event.RxExceptionEvent

abstract class SafeObserver<T> : DisposableObserver<T>() {

    override fun onComplete() {
        dispose()
    }

    override fun onNext(target: T) { /* Default implementation. Can be overridden. */
    }

    override fun onError(ex: Throwable) {
        RxBus.publish(RxExceptionEvent(ex))
        dispose()
    }
}