package io.supercharge.movies.core.util

/**
 * Special singleton creator for kotlin.
 * In kotlin you can have a singleton by using the object keyword.
 * If you want to have a constructor argument, you may use this class as a type of a companion object.
 * Google name of the class for further info because it's a widely used solution.
 */
open class SingletonHolder<out T, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun getInstance(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}