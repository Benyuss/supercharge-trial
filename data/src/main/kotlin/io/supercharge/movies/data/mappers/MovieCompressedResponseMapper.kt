package io.supercharge.movies.data.mappers

import io.supercharge.movies.core.mapper.Mapper
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.network.entities.search.MovieSearchResponse

object MovieCompressedResponseMapper : Mapper<MovieSearchResponse, MovieCompressed> {

    override fun mapFrom(from: MovieSearchResponse): MovieCompressed {
        return MovieCompressed(id = from.id, poster = from.poster, title = from.title)
    }
}