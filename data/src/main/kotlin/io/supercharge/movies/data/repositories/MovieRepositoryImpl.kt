package io.supercharge.movies.data.repositories

import io.reactivex.Maybe
import io.reactivex.Observable
import io.supercharge.movies.data.datastore.cloud.MovieDetailedCloudDataStore
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed
import io.supercharge.movies.domain.repositories.MovieRepository

class MovieRepositoryImpl(
    private val dataStore: MovieDetailedCloudDataStore
) : MovieRepository {

    override fun getByTerm(term: String): Observable<List<MovieCompressed>> {
        return dataStore.getByTerm(term)
    }

    override fun getById(id: Long): Maybe<MovieDetailed> {
        return dataStore.getById(id)
    }
}