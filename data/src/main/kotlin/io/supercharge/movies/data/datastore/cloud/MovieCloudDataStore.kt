package io.supercharge.movies.data.datastore.cloud

import io.reactivex.Maybe
import io.reactivex.Observable
import io.supercharge.movies.data.datastore.parent.MovieDataStore
import io.supercharge.movies.data.mappers.MovieCompressedResponseMapper
import io.supercharge.movies.data.mappers.MovieDetailResponseMapper
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed
import io.supercharge.movies.network.controller.MovieController

class MovieDetailedCloudDataStore(private val controller: MovieController) : MovieDataStore {

    override fun getByTerm(term: String): Observable<List<MovieCompressed>> {
        //todo page management
        return controller.getByTerm(term, 1).map {
            MovieCompressedResponseMapper.mapFromList(it.results)
        }.toObservable()
    }

    override fun getById(id: Long): Maybe<MovieDetailed> {
        return controller.getById(id).map {
            MovieDetailResponseMapper.mapFrom(it)
        }.toMaybe()
    }
}