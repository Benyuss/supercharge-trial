package io.supercharge.movies.data.mappers

import io.supercharge.movies.core.mapper.Mapper
import io.supercharge.movies.domain.entities.Genre
import io.supercharge.movies.network.entities.detail.GenreResponse

object GenreResponseMapper : Mapper<GenreResponse, Genre> {

    override fun mapFrom(from: GenreResponse): Genre {
        return Genre(from.name)
    }
}