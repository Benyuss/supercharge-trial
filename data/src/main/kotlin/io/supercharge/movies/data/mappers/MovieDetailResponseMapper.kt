package io.supercharge.movies.data.mappers

import io.supercharge.movies.core.mapper.Mapper
import io.supercharge.movies.domain.entities.MovieDetailed
import io.supercharge.movies.network.entities.detail.MovieDetailedResponse

object MovieDetailResponseMapper : Mapper<MovieDetailedResponse, MovieDetailed> {

    override fun mapFrom(from: MovieDetailedResponse): MovieDetailed {
        return MovieDetailed(
            budget = from.budget,
            title = from.title,
            poster = from.poster,
            genres = GenreResponseMapper.mapFromList(from.genres),
            overview = from.overview
        )
    }
}