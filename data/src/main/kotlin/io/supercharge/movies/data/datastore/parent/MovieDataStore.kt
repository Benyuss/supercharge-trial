package io.supercharge.movies.data.datastore.parent

import io.reactivex.Maybe
import io.reactivex.Observable
import io.supercharge.movies.domain.datastore.core.DataStore
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed

interface MovieDataStore {

    fun getByTerm(term: String): Observable<List<MovieCompressed>>
    fun getById(id: Long): Maybe<MovieDetailed>
}