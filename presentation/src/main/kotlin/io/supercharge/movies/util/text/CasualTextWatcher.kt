package io.supercharge.movies.util.text

import android.text.Editable
import android.text.TextWatcher

/**
 * TextWatcher to avoid implementation of boilerplate methods
 */
interface CasualTextWatcher : TextWatcher {

    override fun afterTextChanged(p0: Editable?) {
        val convertedString = p0.toString().trim()
        afterTextChanged(convertedString)
    }

    fun afterTextChanged(text: String)

    //region Unused TextWatcher functions. Should remain like this in the future.
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { /*Do nothing */
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { /* Do nothing */
    }
    //endregion
}