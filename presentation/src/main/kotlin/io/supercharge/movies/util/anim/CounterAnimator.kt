package io.supercharge.movies.util.anim

import android.animation.ValueAnimator
import android.widget.TextView
import io.supercharge.movies.common.Constants

fun animateCounterFromInitialValue(view: TextView, count: Int) {
    val animator = ValueAnimator()
    var initialValue = 0
    try {
        initialValue = Integer.parseInt(view.text.toString())
    } catch (e: Exception) {

    }

    animator.setObjectValues(initialValue, count)
    animator.addUpdateListener { animation -> view.text = animation.animatedValue.toString() }
    animator.setEvaluator { fraction, startValue, endValue -> Math.round(startValue as Int + (endValue as Int - startValue) * fraction) }
    animator.duration = Constants.Transition.COUNTER_ANIM_TIME
    animator.start()
}