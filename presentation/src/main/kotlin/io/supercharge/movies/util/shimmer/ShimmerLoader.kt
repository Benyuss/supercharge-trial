package io.supercharge.movies.util.shimmer

import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import java.util.*

abstract class ShimmerLoader {

    var shimmerLayouts = ArrayList<ShimmerFrameLayout>()
    private var isShimming = false


    constructor(shimmerLayout: ShimmerFrameLayout) {
        shimmerLayouts.add(shimmerLayout)
    }

    constructor(shimmerLayouts: ArrayList<ShimmerFrameLayout>) {
        this.shimmerLayouts = shimmerLayouts
    }

    fun startLoading() {
        isShimming = true
        onLoading()

        for (shimmerLayout in shimmerLayouts) {
            val shimmer = Shimmer.AlphaHighlightBuilder()
                .setBaseAlpha(BASE_ALPHA)
                .setHighlightAlpha(HIGHLIGHT_ALPHA)
                .setDuration(BASE_DURATION.toLong())
                .setAutoStart(false)
                .build()
            shimmerLayout.setShimmer(shimmer)
            shimmerLayout.startShimmer()
        }
    }

    fun stopLoading() {
        isShimming = false
        onLoaded()

        for (shimmerLayout in shimmerLayouts) {
            shimmerLayout.stopShimmer()
            shimmerLayout.setShimmer(null)
            shimmerLayout.clearAnimation()
        }
    }

    fun addShimmerLayout(shimmerLayout: ShimmerFrameLayout) {
        shimmerLayouts.add(shimmerLayout)
        if (isShimming) {
            shimmerLayout.startShimmer()
        }
    }

    private fun onLoading() {}

    private fun onLoaded() {}

    companion object {
        private const val BASE_DURATION = 1200
        private const val BASE_ALPHA = 1f
        private const val HIGHLIGHT_ALPHA = 0.2f
    }

}