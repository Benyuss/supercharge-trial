package io.supercharge.movies.util.text

import android.text.Editable
import android.text.TextWatcher
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.supercharge.movies.core.rx.DisposableManager
import java.util.concurrent.TimeUnit

private const val TEXT_WATCHER_DELAY = 200L

class DelayedTextWatcher(private val callback: () -> Unit) : TextWatcher {

    private val disposableManager = DisposableManager()

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        disposableManager.dispose()
    }

    override fun afterTextChanged(s: Editable) {
        disposableManager.add(
            Observable.interval(TEXT_WATCHER_DELAY, TimeUnit.MILLISECONDS)
                .take(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    callback()
                })
    }
}