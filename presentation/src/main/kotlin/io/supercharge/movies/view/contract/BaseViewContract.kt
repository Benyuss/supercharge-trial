package io.supercharge.movies.view.contract

import androidx.appcompat.app.AppCompatActivity

interface BaseViewContract {

    fun getContext(): AppCompatActivity
}