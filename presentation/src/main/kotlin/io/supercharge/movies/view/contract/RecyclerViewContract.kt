package io.supercharge.movies.view.contract

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.supercharge.movies.view.custom.RecyclerViewEmptySupport
import kotlinx.android.synthetic.main.content_list.view.*

interface RecyclerViewContract : BaseViewContract {
    fun initRecyclerView(layoutManager: RecyclerView.LayoutManager)

    fun setRecyclerViewEmptyView(view: View)
}