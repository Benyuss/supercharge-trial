package io.supercharge.movies.common

interface Constants {

    interface Transition {
        companion object {
            const val QUICK_ANIM_TIME: Long = 200
            const val DEFAULT_ANIM_TIME: Long = 400
            const val MEDIUM_ANIM_TIME: Long = 600
            const val SLOW_ANIM_TIME: Long = 800
            const val LONG_ANIM_TIME: Long = 1000

            const val FADE_IN_TIME: Long = 500

            const val COUNTER_ANIM_TIME: Long = 2000
        }
    }

    interface RequestCodes {
        companion object {
            const val PERMISSION_REQUEST = 320
        }
    }

    interface SubActivityCodes {
        companion object
    }

    interface Bundle {
        companion object {
        }
    }
}