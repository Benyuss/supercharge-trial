package io.supercharge.movies.common

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import io.supercharge.movies.BuildConfig
import io.supercharge.movies.di.searchModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MoviesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initThreeTen()
        initLogging()
        initDi()
    }

    private fun initThreeTen() {
        AndroidThreeTen.init(this)
    }

    private fun initLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initDi() {
        startKoin {
            // declare used Android context
            androidContext(this@MoviesApplication)
            // declare modules
            modules(searchModule)
        }
    }
}