package io.supercharge.movies.common

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.supercharge.movies.core.event.RxExceptionEvent
import io.supercharge.movies.core.rx.DisposableManager
import io.supercharge.movies.core.rx.RxBus
import io.supercharge.movies.network.core.ApiError.*
import io.supercharge.movies.network.events.ApiErrorEvent
import io.supercharge.movies.view.contract.BaseViewContract
import timber.log.Timber

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), BaseViewContract {

    protected val disposeBag: DisposableManager = DisposableManager()

    override fun onStart() {
        super.onStart()
        subscribeToRxExceptions()
        subscribeToApiErrors()
    }

    private fun subscribeToRxExceptions() {
        disposeBag.add(RxBus.listen(RxExceptionEvent::class.java).subscribe({
            Timber.tag("Rx event error on acti")
                .e(
                    it.error, it.error.message ?: "No detailed error message can be found."
                )
            it.error.printStackTrace()
        }, {
            Timber.tag("Rx event error on acti").e(it)
        }))
    }

    private fun subscribeToApiErrors() {
        disposeBag.add(RxBus.listen(ApiErrorEvent::class.java).observeOn(AndroidSchedulers.mainThread()).subscribe({
            Timber.tag("API event error on acti").e(it.errorBody)
            when (it.apiError) {
                BAD_RESPONSE -> {
                }
                MALFORMED_JSON -> {
                }
                CONNECTION_TIMEOUT -> {
                }
                SESSION_TIMEOUT -> {
                }
                SERVER_ERROR -> {
                }
                SERVER_UNREACHABLE -> {
                }
            }

        }, {
            Timber.tag("API event error on acti").e(it)
        }))
    }

    override fun getContext(): AppCompatActivity {
        return this
    }

    override fun onStop() {
        super.onStop()
        disposeBag.dispose()
    }


}