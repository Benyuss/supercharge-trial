package io.supercharge.movies.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

object Navigator {

    /**
     * Navigate to the activity given as [clazz] from the starting [activity] and do nothing else.
     *
     * Note: Starter activity does not call finish.
     */
    fun <T> navigateToActivity(activity: AppCompatActivity, clazz: Class<T>) {
        val intent = Intent(activity, clazz)
        activity.startActivity(intent)
    }

    /**
     * Navigate to the activity given as [clazz] from the starting [activity] and finish it.
     */
    fun <T> navigateToActivityAndFinish(activity: AppCompatActivity, clazz: Class<T>) {
        navigateToActivity(activity, clazz)
        activity.finish()
    }

    /**
     * Navigate to the activity given as [clazz] from the starting [activity] and finish it.
     */
    fun navigateToActivityAndFinish(activity: AppCompatActivity, intent: Intent) {
        activity.startActivity(intent)
        activity.finish()
    }

    /**
     * Navigate to the activity given as [clazz] from the starting [activity] and finish it.
     */
    fun <T> navigateToActivityAndFinishWithoutHistory(activity: AppCompatActivity, clazz: Class<T>) {
        val intent = Intent(activity, clazz)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        activity.finish()
        activity.startActivity(intent)
    }

    fun recreateActivity(activity: AppCompatActivity) {
        val intent = Intent(activity, activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.finish()
        activity.startActivity(intent)
    }

    private fun ensureHttpUri(url: String): String {
        var validUri: String = url
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            validUri = "http://$url"
        }
        return validUri
    }
}