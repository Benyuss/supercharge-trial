package io.supercharge.movies.di

import org.koin.dsl.module

class AppComponent

@JvmField
val appModule = module {
    single { AppComponent() }
}
