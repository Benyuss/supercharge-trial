package io.supercharge.movies.di

import io.supercharge.movies.data.datastore.cloud.MovieDetailedCloudDataStore
import io.supercharge.movies.data.repositories.MovieRepositoryImpl
import io.supercharge.movies.domain.interactor.MovieInteractor
import io.supercharge.movies.domain.repositories.MovieRepository
import io.supercharge.movies.movies.search.logic.MovieSearchPresenterImpl
import io.supercharge.movies.movies.search.logic.MovieSearchViewModel
import io.supercharge.movies.movies.search.view.MovieSearchContract
import io.supercharge.movies.network.controller.MovieController
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchModule = module {
    factory { MovieController() }
    factory { MovieDetailedCloudDataStore(get()) }
    single { MovieRepositoryImpl(get()) as MovieRepository }
    factory { MovieInteractor(get()) }
    viewModel { MovieSearchViewModel(get()) }
    factory { (view: MovieSearchContract.MovieSearchViewContract) ->
        MovieSearchPresenterImpl(
            view,
            get()
        ) as MovieSearchContract.MovieSearchPresenter
    }
}