package io.supercharge.movies.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

fun isPermissionGranted(permission: String, context: Context): Boolean =
    ContextCompat.checkSelfPermission(
        context,
        permission
    ) == PackageManager.PERMISSION_GRANTED

fun Activity.showPermissionReasonAndRequest(
    title: String,
    message: String,
    permissions: Array<String>,
    requestCode: Int
) {
    AlertDialog.Builder(this)
        .setMessage(message)
        .setTitle(title)
        .setPositiveButton(android.R.string.yes) { _, _ ->
            ActivityCompat.requestPermissions(
                this@showPermissionReasonAndRequest,
                permissions,
                requestCode
            )
        }
        .setNegativeButton(android.R.string.no) { dialog, _ ->
            dialog.dismiss()
        }.show()
}

fun Activity.navigateToSettings() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", this.packageName, null)
    intent.data = uri
    startActivity(intent)
}