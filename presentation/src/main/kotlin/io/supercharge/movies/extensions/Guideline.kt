package io.supercharge.movies.extensions

import android.animation.ValueAnimator
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import io.supercharge.movies.common.Constants

fun Guideline.setGuideLinePercentAnimated(percent: Float) {
    val start = (layoutParams as ConstraintLayout.LayoutParams).guidePercent
    // get end percent. start at 0
    val valueAnimator = ValueAnimator.ofFloat(start, percent)
    valueAnimator.duration = Constants.Transition.MEDIUM_ANIM_TIME
    // set duration
    valueAnimator.interpolator = AccelerateDecelerateInterpolator()
    // set interpolator and  updateListener to get the animated value
    valueAnimator.addUpdateListener { valueAnimator ->
        val lp = layoutParams as ConstraintLayout.LayoutParams
        // get the float value
        lp.guidePercent = valueAnimator.animatedValue as Float
        // update layout params
        layoutParams = lp
    }
    valueAnimator.start()
}