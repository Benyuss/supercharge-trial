package io.supercharge.movies.extensions

import android.view.WindowManager
import androidx.fragment.app.Fragment

fun Fragment.setStatusBarGradient(gradientRes: Int) {
    val window = activity?.window
    val background = resources.getDrawable(gradientRes) //this is our gradient
    window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window?.statusBarColor = resources.getColor(android.R.color.transparent)
    window?.setBackgroundDrawable(background)
}