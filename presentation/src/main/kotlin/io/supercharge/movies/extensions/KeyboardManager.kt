package io.supercharge.movies.extensions

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity

fun hideKeyboard(activity: AppCompatActivity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    activity.currentFocus?.let { view ->
        view.clearFocus()
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}