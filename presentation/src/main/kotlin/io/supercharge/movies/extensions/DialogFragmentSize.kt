package io.supercharge.movies.extensions

import android.view.ViewGroup
import androidx.fragment.app.DialogFragment

fun DialogFragment.adjustSize(widthPercent: Float?, heightPercent: Float?) {
    var finalWidth = ViewGroup.LayoutParams.WRAP_CONTENT
    var finalHeight = ViewGroup.LayoutParams.WRAP_CONTENT

    widthPercent?.apply {
        finalWidth = ((context?.resources?.displayMetrics?.widthPixels ?: 0).toFloat() * this).toInt()
    }

    heightPercent?.apply {
        finalHeight = ((context?.resources?.displayMetrics?.heightPixels ?: 0).toFloat() * this).toInt()
    }

    dialog?.window?.setLayout(finalWidth, finalHeight)
}