package io.supercharge.movies.extensions

import android.graphics.drawable.Drawable
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.getView(): View {
    return window.decorView.findViewById(android.R.id.content)
}

fun AppCompatActivity.setStatusBarGradient(gradientRes: Int) {
    val background = resources.getDrawable(gradientRes) //this is our gradient
    window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window?.statusBarColor = resources.getColor(android.R.color.transparent)
    window?.setBackgroundDrawable(background)
}

fun AppCompatActivity.setStatusBarGradient(gradient: Drawable) {
    window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window?.statusBarColor = resources.getColor(android.R.color.transparent)
    window?.setBackgroundDrawable(gradient)
}