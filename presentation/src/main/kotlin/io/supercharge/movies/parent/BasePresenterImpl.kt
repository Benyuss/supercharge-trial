package io.supercharge.movies.parent

import android.os.Bundle
import androidx.lifecycle.Observer
import io.supercharge.movies.core.rx.DisposableManager
import io.supercharge.movies.view.contract.BaseViewContract
import timber.log.Timber

abstract class BasePresenterImpl<T>(
    protected open val view: BaseViewContract,
    protected open val viewModel: BaseViewModel<T>
) : BasePresenter<T> {

    protected val disposableManager = DisposableManager()

    private val viewStateObserver: Observer<T> = Observer { state ->
        if (state != null) handleViewState(state)
    }

    private val errorStateObserver: Observer<in Throwable?> = Observer { throwable ->
        throwable?.let { error ->
            handleErrorState(error)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        doOnCreate()
        Timber.d("Presenter created.")
    }

    protected open fun doOnCreate() {}

    override fun onViewCreated(bundle: Bundle?) {
        doOnViewCreated()
    }

    protected open fun doOnViewCreated() {}

    override fun handleErrorState(error: Throwable) {
        Timber.tag("ViewState Error").e(error, error.message ?: "No detailed error message can be found.")
        error.printStackTrace()
    }

    override fun onStart() {
        subscribeToVMChanges()
        doOnStart()
        Timber.d("Presenter started.")
    }

    abstract fun doOnStart()

    override fun subscribeToVMChanges() {
        viewModel.viewState.observe(view.getContext(), viewStateObserver)
        viewModel.errorState.observe(view.getContext(), errorStateObserver)
    }

    override fun onStop() {
        disposableManager.dispose()
        viewModel.viewState.removeObserver(viewStateObserver)
        doOnStop()
        Timber.d("Presenter stopped.")
    }

    protected open fun doOnStop() {}

    override fun onDestroy() {
        doOnDestroy()
        Timber.d("Presenter destroyed.")
    }

    protected open fun doOnDestroy() {}
}