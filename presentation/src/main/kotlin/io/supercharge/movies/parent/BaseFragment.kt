package io.supercharge.movies.parent

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import io.supercharge.movies.view.contract.BaseViewContract
import timber.log.Timber

abstract class BaseFragment<T> : Fragment(), BaseViewContract {

    protected abstract val presenter: BasePresenter<T>

    override fun getContext(): AppCompatActivity {
        return requireActivity() as AppCompatActivity
    }

    open fun onBackPressed(): Boolean {
        //nothing to do subclasses can override it
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(arguments)
        Timber.d("Fragment created.")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
        Timber.d("Fragment started.")
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
        Timber.d("Fragment resumed.")
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
        Timber.d("Fragment paused.")
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
        Timber.d("Fragment stopped.")
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
        Timber.d("Fragment destroyed.")
    }
}