package io.supercharge.movies.movies.search.logic

import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import io.supercharge.movies.R
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.extensions.isNotNullOrEmpty
import io.supercharge.movies.movies.search.view.MovieSearchContract
import io.supercharge.movies.parent.BasePresenterImpl
import io.supercharge.movies.util.text.DelayedTextWatcher

class MovieSearchPresenterImpl(
    override var view: MovieSearchContract.MovieSearchViewContract,
    override val viewModel: MovieSearchViewModel

) : BasePresenterImpl<MovieSearchViewState>(view, viewModel), MovieSearchContract.MovieSearchPresenter {

    private var adapter: MovieAdapter? = null

    override fun doOnCreate() {
        adapter = MovieAdapter(view.getContext(), arrayListOf()) {
            viewModel.getBudgetInfo(it)
        }
    }

    override fun doOnStart() {
        initRecyclerView()
        fetchMovies()
        view.setSearchWatcher(getWatcherForSearchChanges())
    }

    private fun initRecyclerView() {
        //todo grid in landscape
        view.initRecyclerView(LinearLayoutManager(view.getContext()))

        adapter?.let {
            view.setListAdapter(it)
        }
    }

    override fun fetchMovies() {
        val searchTerm = view.getSearchTerm()
        if (searchTerm.isNotNullOrEmpty()) {
            viewModel.getCompressedResults(searchTerm)
        } else {
            if (viewModel.isViewInitialized()) {
                view.showEmptyTermError(view.getContext().getString(R.string.error_empty_search))
            } else {
                view.showUninitializedState(view.getContext().getString(R.string.provide_term))
            }
        }
    }

    override fun getWatcherForSearchChanges(): TextWatcher {
        return DelayedTextWatcher {
            fetchMovies()
        }
    }

    override fun handleViewState(state: MovieSearchViewState) {
        handleLoadingState(state.isLoading)
        handleListState(state.movies)
    }

    private fun handleLoadingState(isLoading: Boolean) {
        view.setLoadingVisibility(
            if (isLoading) {
                View.VISIBLE
            } else {
                View.GONE
            }
        )
    }

    private fun handleListState(movies: List<MovieCompressed>?) {
        movies?.let {
            adapter?.setMovies(it)
        }
    }

    override fun handleErrorState(error: Throwable) {
        super.handleErrorState(error)
        view.showNetWorkError(view.getContext().getString(R.string.error_data_fetch))
    }
}