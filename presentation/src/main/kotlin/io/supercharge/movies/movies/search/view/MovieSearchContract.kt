package io.supercharge.movies.movies.search.view

import android.text.TextWatcher
import io.supercharge.movies.parent.BasePresenter
import io.supercharge.movies.movies.search.logic.MovieAdapter
import io.supercharge.movies.movies.search.logic.MovieSearchViewState
import io.supercharge.movies.view.contract.RecyclerViewContract

interface MovieSearchContract {

    interface MovieSearchViewContract : RecyclerViewContract {
        fun setListAdapter(adapter: MovieAdapter)

        fun getSearchTerm(): String?
        fun setSearchWatcher(textWatcher: TextWatcher)

        fun showEmptyTermError(error: String)
        fun showUninitializedState(message: String)
        fun showNetWorkError(message: String)

        fun setLoadingVisibility(visibility: Int)
    }

    interface MovieSearchPresenter : BasePresenter<MovieSearchViewState> {

        fun getWatcherForSearchChanges(): TextWatcher

        fun fetchMovies()

    }
}