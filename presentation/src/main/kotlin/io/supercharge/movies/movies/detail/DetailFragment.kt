package io.supercharge.movies.movies.detail

import android.os.Bundle
import androidx.fragment.app.Fragment

class DetailFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            val movieId = DetailFragmentArgs.fromBundle(it).movieId

            //todo start detail page with that particular id
        }
    }
}