package io.supercharge.movies.movies.search.view

import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import io.supercharge.movies.R
import io.supercharge.movies.movies.search.logic.MovieAdapter
import io.supercharge.movies.movies.search.logic.MovieSearchViewState
import io.supercharge.movies.parent.BaseFragment
import kotlinx.android.synthetic.main.content_list.*
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class SearchFragment : BaseFragment<MovieSearchViewState>(), MovieSearchContract.MovieSearchViewContract {

    override val presenter: MovieSearchContract.MovieSearchPresenter by inject { parametersOf(this@SearchFragment) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun initRecyclerView(layoutManager: RecyclerView.LayoutManager) {
        list?.layoutManager = layoutManager
    }

    override fun setRecyclerViewEmptyView(view: View) {
        //todo invoke
        list?.setEmptyView(view)
    }

    override fun setListAdapter(adapter: MovieAdapter) {
        list?.adapter = adapter
    }

    override fun getSearchTerm(): String? {
        return searchInput?.text?.toString()
    }

    override fun setSearchWatcher(textWatcher: TextWatcher) {
        searchInput?.addTextChangedListener(textWatcher)
    }

    override fun showEmptyTermError(error: String) {
        //todo empty screen
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
    }

    override fun showUninitializedState(message: String) {
        //todo unique empty state screen
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showNetWorkError(message: String) {
        //todo custom error for network errors.
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun setLoadingVisibility(visibility: Int) {
        searchProgress?.visibility = visibility
    }
}