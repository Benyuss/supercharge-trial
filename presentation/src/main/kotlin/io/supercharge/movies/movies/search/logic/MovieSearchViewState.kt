package io.supercharge.movies.movies.search.logic

import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed

data class MovieSearchViewState(
    var searchedAtLeastOnce: Boolean = false,
    var isLoading: Boolean = false,
    var movies: List<MovieCompressed>? = null,
    var moviesDetailed: Map<Long, MovieDetailed>? = null
)