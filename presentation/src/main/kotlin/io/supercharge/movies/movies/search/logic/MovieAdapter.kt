package io.supercharge.movies.movies.search.logic

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.supercharge.movies.R
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.extensions.navigateSafe
import io.supercharge.movies.movies.search.view.SearchFragmentDirections
import io.supercharge.movies.network.common.ApiConstants
import kotlinx.android.synthetic.main.item_movie.view.*
import timber.log.Timber

class MovieAdapter(
    private val context: Context,
    private var dataSet: List<MovieCompressed>,
    private var loadItemCallBack: (movieId: Long) -> Observable<Long>
) :
    RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
        return MovieViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = dataSet[position]
        holder.name?.text = movie.title
        loadImage(holder.image, movie.poster)
        setNavigationListener(holder.clickTrap, movie.id)
        loadBudget(holder.budget, movie.id)

    }

    @SuppressLint("CheckResult")
    private fun loadBudget(textView: TextView?, movieId: Long) {
        loadItemCallBack(movieId).observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                textView?.text = String.format(context.getString(R.string.budget), it)
            }, {
                Timber.e(it, "Can not load budget data for movie $movieId")
            })
    }

    private fun setNavigationListener(clickTrap: FrameLayout?, id: Long) {
        clickTrap?.setOnClickListener {
            Navigation.findNavController(context as AppCompatActivity, R.id.movieHostFragment)
                .navigateSafe(SearchFragmentDirections.actionSearchFragmentToDetailFragment(id))
        }
    }

    private fun loadImage(imageHolder: ImageView?, poster: String) {
        imageHolder?.run {
            //todo dynamic resize
            Glide.with(context).load(getImageUrl(poster)).apply(RequestOptions.overrideOf(350)).into(this)
        }
    }

    private fun getImageUrl(poster: String): String {
        return ApiConstants.POSTER_BASE_URL + poster
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    //todo page handling
    fun setMovies(movies: List<MovieCompressed>) {
        dataSet = movies
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var name: TextView? = itemView.movieName
        var image: ImageView? = itemView.movieImage

        var budget: TextView? = itemView.movieBudget

        var clickTrap: FrameLayout = itemView.movieClickTrap

        //todo some sort of loading - shimmer for the budget
    }
}