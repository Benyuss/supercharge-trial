package io.supercharge.movies.movies.search.logic

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.supercharge.movies.domain.interactor.MovieInteractor
import io.supercharge.movies.parent.BaseViewModel
import timber.log.Timber

class MovieSearchViewModel(
    private val interactor: MovieInteractor
) : BaseViewModel<MovieSearchViewState>() {

    init {
        viewState.value = MovieSearchViewState()
    }

    fun getCompressedResults(searchTerm: String) {
        viewState.value = viewState.value?.copy(isLoading = true, searchedAtLeastOnce = true)
        addDisposable(
            interactor.getByTerm(searchTerm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.value = viewState.value?.copy(isLoading = false, movies = it)
                }, {
                    Timber.e(it, "Movie search fetch unsuccessful.")
                    viewState.value = viewState.value?.copy(isLoading = false)
                    errorState.value = it
                })
        )
    }

    //todo
    fun getBudgetInfo(id: Long): Observable<Long> {
        return interactor.getById(id).map { it.budget }.subscribeOn(Schedulers.io()).toObservable()
    }

    fun isViewInitialized(): Boolean {
        return viewState.value?.searchedAtLeastOnce ?: false
    }
}