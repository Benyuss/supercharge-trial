package io.supercharge.movies.network.listeners

interface OnConnectionErrorListener {

    fun onMalformedJsonResponse()
    fun onServerUnreachable(errorBody: String?)
    fun onConnectionTimeout()
    fun onSessionTimeout(errorBody: String)
    fun onServerError(errorBody: String?)
}