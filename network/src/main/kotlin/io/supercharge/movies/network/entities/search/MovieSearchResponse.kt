package io.supercharge.movies.network.entities.search

import com.squareup.moshi.Json

data class MovieSearchResponse (
    var id : Long,
    var title : String,
    @Json(name = "poster_path")
    var poster : String
)