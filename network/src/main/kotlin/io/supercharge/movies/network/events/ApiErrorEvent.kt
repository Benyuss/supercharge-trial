package io.supercharge.movies.network.events

import io.supercharge.movies.network.core.ApiError

class ApiErrorEvent(val apiError: ApiError, val errorBody: String? = "")