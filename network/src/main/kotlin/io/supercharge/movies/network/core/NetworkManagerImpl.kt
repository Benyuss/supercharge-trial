package io.supercharge.movies.network.core

import android.content.Context
import android.net.ConnectivityManager
import io.supercharge.movies.core.util.NetworkManager

class NetworkManagerImpl(private var context: Context) : NetworkManager {

    override fun isConnectionAvailable(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}