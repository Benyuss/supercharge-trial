package io.supercharge.movies.network.core

enum class ApiError {
    BAD_RESPONSE, MALFORMED_JSON, SESSION_TIMEOUT, CONNECTION_TIMEOUT, SERVER_ERROR, SERVER_UNREACHABLE;
}