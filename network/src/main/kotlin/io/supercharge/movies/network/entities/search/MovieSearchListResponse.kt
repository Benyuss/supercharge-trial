package io.supercharge.movies.network.entities.search

import com.squareup.moshi.Json

data class MovieSearchListResponse(
    var page : Int,
    @Json(name = "total_pages")
    var totalPages : Int,
    var results: List<MovieSearchResponse>
)