package io.supercharge.movies.network.api

import io.reactivex.Single
import io.supercharge.movies.network.entities.detail.MovieDetailedResponse
import io.supercharge.movies.network.entities.search.MovieSearchListResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("search/movie")
    fun getByTerm(@Query("query") term: String, @Query("page") page: Int) : Single<MovieSearchListResponse>

    @GET("movie/{id}")
    fun getById(@Path("id") id: Long) : Single<MovieDetailedResponse>
}