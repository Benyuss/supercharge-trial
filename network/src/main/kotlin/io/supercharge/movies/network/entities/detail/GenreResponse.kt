package io.supercharge.movies.network.entities.detail

data class GenreResponse(
    var name: String
)