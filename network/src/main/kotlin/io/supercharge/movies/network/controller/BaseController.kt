package io.supercharge.movies.network.controller

import io.supercharge.movies.core.rx.RxBus
import io.supercharge.movies.network.core.ApiError
import io.supercharge.movies.network.core.HttpClientFactory
import io.supercharge.movies.network.events.ApiErrorEvent
import io.supercharge.movies.network.listeners.OnConnectionErrorListener
import okhttp3.OkHttpClient

abstract class BaseController {

    private val errorListener: OnConnectionErrorListener = object : OnConnectionErrorListener {
        override fun onServerUnreachable(errorBody: String?) {
            RxBus.publish(ApiErrorEvent(ApiError.SERVER_UNREACHABLE, errorBody))
        }

        override fun onMalformedJsonResponse() {
            RxBus.publish(ApiErrorEvent(ApiError.MALFORMED_JSON))
        }

        override fun onSessionTimeout(errorBody: String) {
            RxBus.publish(ApiErrorEvent(ApiError.SESSION_TIMEOUT, errorBody))
        }

        override fun onConnectionTimeout() {
            RxBus.publish(ApiErrorEvent(ApiError.CONNECTION_TIMEOUT))
        }

        override fun onServerError(errorBody: String?) {
            RxBus.publish(ApiErrorEvent(ApiError.SERVER_ERROR, errorBody))
        }

    }

    protected fun getHttpClient(customListener: OnConnectionErrorListener = errorListener): OkHttpClient {
        return HttpClientFactory(customListener).httpClient
    }
}