package io.supercharge.movies.network.core

import android.util.Log
import com.squareup.moshi.JsonDataException
import io.supercharge.movies.network.common.ApiConstants
import io.supercharge.movies.network.listeners.OnConnectionErrorListener
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class HttpClientFactory(
    private var onConnectionErrorListener: OnConnectionErrorListener
) {

    val httpClient: OkHttpClient by lazy { makeHttpClient() }

    private fun makeHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor { chain -> errorHandlerInterceptor(chain) }
            .addInterceptor { chain -> addApiKeyHeaderInterceptor(chain) }

        setDebugModeInterceptor(client)

        return client.build()
    }

    private fun addApiKeyHeaderInterceptor(
        chain: Interceptor.Chain
    ): Response {
        val originalRequest = chain.request()
        val builder = originalRequest.url().newBuilder().addQueryParameter("api_key", ApiConstants.API_KEY)
        val newRequest = originalRequest.newBuilder().url(builder.build()).build()
        return chain.proceed(newRequest)
    }

    private fun setDebugModeInterceptor(client: OkHttpClient.Builder) {
        val interceptor = HttpLoggingInterceptor()

        interceptor.level = HttpLoggingInterceptor.Level.BODY
        if (ApiConstants.LOG_NETWORK) {
            client.addInterceptor(interceptor)
        }
    }

    @Throws(IOException::class)
    private fun errorHandlerInterceptor(chain: Interceptor.Chain): Response? {
        try {
            val request = chain.request()
            val response = chain.proceed(request)

            when {
                response.code() == 401 -> {
                    val responseBodyCopy = response.peekBody(Long.MAX_VALUE)
                    Log.e(HttpClientFactory::class.java.simpleName, "Session invalid")
                    onConnectionErrorListener.onSessionTimeout(responseBodyCopy.string())
                }
                response.code() == 404 -> {
                    val responseBodyCopy = response.peekBody(Long.MAX_VALUE)
                    Log.e(HttpClientFactory::class.java.simpleName, "Unreachable server")
                    onConnectionErrorListener.onServerUnreachable(responseBodyCopy.string())
                }
                !response.isSuccessful -> {
                    val responseBodyCopy = response.peekBody(Long.MAX_VALUE)
                    Log.e(HttpClientFactory::class.java.simpleName, "Server error")
                    onConnectionErrorListener.onServerError(responseBodyCopy.string())
                }
            }

            return response
        } catch (exception: SocketTimeoutException) {
            Log.e(HttpClientFactory::class.java.simpleName, "Socket timeout")
            exception.printStackTrace()
            onConnectionErrorListener.onConnectionTimeout()
        } catch (exception: JsonDataException) {
            Log.e(HttpClientFactory::class.java.simpleName, "Malformed JSON")
            onConnectionErrorListener.onMalformedJsonResponse()
        } catch (exception: Exception) {
            Log.e(HttpClientFactory::class.java.simpleName, "Generic error")
            // When user cancel any webservice call, error handler should not be notified
            if (exception.message?.contains("Canceled") == true) {
                return null
            }
            onConnectionErrorListener.onServerError("")
        }

        return null
    }
}