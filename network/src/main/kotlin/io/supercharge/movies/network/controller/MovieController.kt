package io.supercharge.movies.network.controller

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.supercharge.movies.network.api.MovieApi
import io.supercharge.movies.network.core.ApiClient
import io.supercharge.movies.network.entities.detail.MovieDetailedResponse
import io.supercharge.movies.network.entities.search.MovieSearchListResponse

class MovieController : BaseController() {

    fun getByTerm(term: String, page: Int): Single<MovieSearchListResponse> {
        val api = ApiClient.getClient(getHttpClient()).create(MovieApi::class.java)

        return api.getByTerm(term, page)
            .subscribeOn(Schedulers.io())
    }

    fun getById(id: Long): Single<MovieDetailedResponse> {
        val api = ApiClient.getClient(getHttpClient()).create(MovieApi::class.java)

        return api.getById(id)
            .subscribeOn(Schedulers.io())
    }
}