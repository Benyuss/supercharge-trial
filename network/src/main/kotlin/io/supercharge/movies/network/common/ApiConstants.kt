package io.supercharge.movies.network.common

object ApiConstants {

    const val LOG_NETWORK = true

    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val POSTER_BASE_URL = "https://image.tmdb.org/t/p/w185"

    const val API_KEY = "43a7ea280d085bd0376e108680615c7f"
}