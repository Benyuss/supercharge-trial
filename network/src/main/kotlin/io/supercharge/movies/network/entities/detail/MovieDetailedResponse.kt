package io.supercharge.movies.network.entities.detail

import com.squareup.moshi.Json

data class MovieDetailedResponse (
    var budget : Long,
    var overview: String,
    var genres : List<GenreResponse>,
    var title : String,
    @Json(name = "poster_path")
    var poster : String
)