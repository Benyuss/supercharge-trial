package io.supercharge.movies.domain.repositories

import io.reactivex.Maybe
import io.reactivex.Observable
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed

interface MovieRepository {

    fun getByTerm(term : String) : Observable<List<MovieCompressed>>

    fun getById(id: Long) : Maybe<MovieDetailed>
}