package io.supercharge.movies.domain.entities

data class Genre(
    var name: String
)