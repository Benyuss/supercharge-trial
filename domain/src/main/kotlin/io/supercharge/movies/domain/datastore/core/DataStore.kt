package io.supercharge.movies.domain.datastore.core

/**
Absolute crud methods which must be supported by every data store type
 */
interface DataStore<T>