package io.supercharge.movies.domain.datastore.core

import io.reactivex.Completable
import io.reactivex.Observable

interface LocalDataStore<T> : DataStore<T> {

    fun save(target: Observable<T>): T
    fun saveAll(target: Observable<List<T>>): Completable

    fun clear()
}