package io.supercharge.movies.domain.entities

data class MovieDetailed(
    var budget: Long,
    var overview: String,
    var genres: List<Genre>,
    var title: String,
    var poster: String
)