package io.supercharge.movies.domain.interactor

import io.reactivex.Maybe
import io.reactivex.Observable
import io.supercharge.movies.domain.entities.MovieCompressed
import io.supercharge.movies.domain.entities.MovieDetailed
import io.supercharge.movies.domain.repositories.MovieRepository

class MovieInteractor(private val repository: MovieRepository) {

    fun getByTerm(term: String): Observable<List<MovieCompressed>> {
        return repository.getByTerm(term)
    }

    fun getById(id: Long): Maybe<MovieDetailed> {
        return repository.getById(id)
    }
}