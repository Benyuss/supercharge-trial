package io.supercharge.movies.domain.datastore.core

interface CloudDataStore<T> : DataStore<T>