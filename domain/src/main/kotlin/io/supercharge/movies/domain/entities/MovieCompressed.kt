package io.supercharge.movies.domain.entities

data class MovieCompressed(
    var id : Long,
    var title : String,
    var poster : String
)