package io.supercharge.movies.domain.datastore.core

import io.supercharge.movies.core.entities.Optional
import io.reactivex.Completable
import io.reactivex.Observable

interface Cache<T> {

    fun clear()
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean

    fun get(id: Long): Observable<Optional<T>>
    fun save(target: Observable<T>): T
    fun remove(id: Long)

    fun getAll(): Observable<List<T>>
    fun saveAll(target: Observable<List<T>>): Completable

    fun isExpired(): Boolean
}